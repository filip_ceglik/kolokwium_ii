﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;

namespace Kolokwium_II
{
    class Program
    {
        static void Main(string[] args)
        {
            //Console.WriteLine("Hello World!");

            #region Zad1
                
                var rand = new Random();
                IEnumerable<DateTime> dat = new[] {new DateTime((long)rand.Next()*rand.Next())};
                IEnumerable<DateTime> datki = new DateTime[200];
                foreach (var item in datki)
                {
                    item.AddTicks((long) rand.Next() * rand.Next()); //robi randomowe?
                }
                var fridays = datki.Select(x => x.DayOfWeek == DayOfWeek.Friday);
                var sorted = datki.OrderBy(x => x.Millisecond);
                foreach (var item in datki) Console.WriteLine(item.Day + item.Month + item.Year);
        
            #endregion


        }
    }
}