using System;

namespace Kolokwium_II
{
    public class Product
    {
        
        public string nazwa, opis;
        public int ilosc;
        public DateTime dataWaznosci;
        public float cena;
        public void Deconstruct(out string nazwa, out string opis, out string dataWaznosci, out float cena)
        {
            nazwa = this.nazwa;
            opis = this.opis;
            if (DateTime.Today.AddDays(7) <= this.dataWaznosci)
            {
                dataWaznosci = ("MNIEJ NIZ 7 DNI");
            }
            else if (DateTime.Today.AddDays(1) <= this.dataWaznosci)
            {
                dataWaznosci = ("MNIEJ NIZ 1 DZIEN");
            }
            else
            {
                dataWaznosci = Convert.ToString(this.dataWaznosci);
            }

            ;
            cena = this.cena;
        }

        public void Deconstruct(out string nazwa, out float cena)
        {
            nazwa = this.nazwa;
            cena = this.cena;
        }

        public Product(string nazwa, string opis, int ilosc, DateTime dataWaznosci, float cena)
        {
            this.nazwa = nazwa;
            this.opis = opis;
            this.ilosc = ilosc;
            this.dataWaznosci = dataWaznosci;
            this.cena = cena;
        }
    }
}